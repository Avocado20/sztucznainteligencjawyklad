
;;;======================================================
;;;   Zakup pralki
;;;
;;;     System ekspercki pomagajacy przy zakupie
;;;     PRALKI dostosowany do twoich potrzeb
;;;
;;;======================================================

;;; ***************************
;;; * DEFTEMPLATES & DEFFACTS *
;;; ***************************

(deftemplate UI-state
   (slot id (default-dynamic (gensym*)))
   (slot display)
   (slot relation-asserted (default none))
   (slot response (default none))
   (multislot valid-answers)
   (slot state (default middle)))
   
(deftemplate state-list
   (slot current)
   (multislot sequence))
  
(deffacts startup
   (state-list)
   (licznik_oznak 0))
   
;;;****************
;;;* STARTUP RULE *
;;;****************

(defrule system-banner ""

  =>
  
  (assert (UI-state (display WelcomeMessage)
                    (relation-asserted start)
                    (state initial)
                    (valid-answers))))

;;;***************
;;;* QUERY RULES *
;;;***************

;;;-----------------------------------------------------------------------------
;;;------------------------------- PYTANIA --------------------------------------
;;;-----------------------------------------------------------------------------

(defrule determine-rocznik ""

   (logical (start))

   =>

   (assert (UI-state (display Pytanie1)
                     (relation-asserted rocznik)
                     (response No)
                     (valid-answers No Yes))))

;;;----KONKLUZJA BRAK WYMIANY-----
					 
(defrule brakWymiany ""

   (logical (rocznik Yes))
   =>

   (assert (UI-state (display KonkluzjabrakWymiany)
                     (state final))))	
					 
;;;----PODSTAWOWE WYMAGANIA-----					 
					 
(defrule determine-PodstPytania ""

   (logical (rocznik No))

   =>

   (assert (UI-state (display Pytanie2)
                     (relation-asserted PodstPytania)
                     (response Yes)
                     (valid-answers No Yes))))

(defrule determine-typ ""

	 (logical (rocznik No))
	 (logical (PodstPytania Yes))
   =>

   (assert (UI-state (display Pytanie3)
                     (relation-asserted typ)
                     (response No)
                     (valid-answers No Yes)))) 

(defrule determine-ladownosc ""

	 (logical (rocznik No))
	 (logical (PodstPytania Yes))
   =>

   (assert (UI-state (display Pytanie4)
                     (relation-asserted ladownosc)
                     (response Yes)
                     (valid-answers No Yes)))) 			

(defrule determine-oszczednosc ""

	 (logical (rocznik No))
	 (logical (PodstPytania Yes))
   =>

   (assert (UI-state (display Pytanie5)
                     (relation-asserted oszczednosc)
                     (response No)
                     (valid-answers No Yes)))) 	

;;;----KONKLUZJA PODSTAWOWE PYTANIA-----	

(defrule determine-PrzodLadownoscOszczednosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ Yes))
   (logical (ladownosc Yes))
   (logical (oszczednosc Yes))
   =>

   (assert (UI-state (display KonkluzjaPrzodLadownoscOszczednosc)
                     (state final))))		

(defrule determine-BrakPrzodLadownoscOszczednosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ No))
   (logical (ladownosc No))
   (logical (oszczednosc No))
   =>

   (assert (UI-state (display KonkluzjaBrakPrzodLadownoscOszczednosc)
                     (state final))))	

(defrule determine-Przod ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ Yes))
   (logical (ladownosc No))
   (logical (oszczednosc No))
   =>

   (assert (UI-state (display KonkluzjaPrzod)
                     (state final))))		

(defrule determine-Ladownosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ No))
   (logical (ladownosc Yes))
   (logical (oszczednosc No))
   =>

   (assert (UI-state (display KonkluzjaLadownosc)
                     (state final))))		

(defrule determine-Oszczednosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ No))
   (logical (ladownosc No))
   (logical (oszczednosc Yes))
   =>

   (assert (UI-state (display KonkluzjaOszczednosc)
                     (state final))))	

(defrule determine-PrzodLadownosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ Yes))
   (logical (ladownosc Yes))
   (logical (oszczednosc No))
   =>

   (assert (UI-state (display KonkluzjaPrzodLadownosc)
                     (state final))))						 
					 
(defrule determine-LadownoscOszczednosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ No))
   (logical (ladownosc Yes))
   (logical (oszczednosc Yes))
   =>

   (assert (UI-state (display KonkluzjaLadownoscOszczednosc)
                     (state final))))	

(defrule determine-PrzodOszczednosc ""

   (logical (rocznik No))
   (logical (PodstPytania Yes))
   (logical (typ Yes))
   (logical (ladownosc No))
   (logical (oszczednosc Yes))
   =>

   (assert (UI-state (display KonkluzjaPrzodOszczednosc)
                     (state final))))		

;;;----PYTANIA O DODATKOWE PROGRAMY PIORACE-----				 
					 
(defrule determine-funkcjonalnosc ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 
   =>

   (assert (UI-state (display Pytanie6)
                     (relation-asserted funkcjonalnosc)
                     (response No)
                     (valid-answers No Yes)))) 	
					 

(defrule determine-funkcjonalnosc1 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc Yes))
 
   =>

   (assert (UI-state (display Pytanie7)
                     (relation-asserted funkcjonalnosc1)
                     (response Yes)
                     (valid-answers No Yes)))) 		

(defrule determine-funkcjonalnosc2 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc Yes))

   =>

   (assert (UI-state (display Pytanie8)
                     (relation-asserted funkcjonalnosc2)
                     (response Yes)
                     (valid-answers No Yes)))) 				


(defrule determine-funkcjonalnosc3 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc Yes))
 
   =>

   (assert (UI-state (display Pytanie9)
                     (relation-asserted funkcjonalnosc3)
                     (response No)
                     (valid-answers No Yes))))
					 
;;;----KONKLUZJE DOTYCZACE DODATKOWYCH PROGRAMOW PIORACYCH----	

(defrule ProgOdziezDelikatna ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 Yes))
   (logical (funkcjonalnosc2 No))
   (logical (funkcjonalnosc3 No))
   =>

   (assert (UI-state (display KonkluzjaPranieOdziezyDelikatnej)
                     (state final))))					 
					 
(defrule ProgPranieReczne ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 No))
   (logical (funkcjonalnosc2 Yes))
   (logical (funkcjonalnosc3 No))
   =>

   (assert (UI-state (display KonkluzjaPranieReczne)
                     (state final))))	
					 
(defrule ProgSzostyZmysl ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 No))
   (logical (funkcjonalnosc2 No))
   (logical (funkcjonalnosc3 Yes))
   =>

   (assert (UI-state (display KonkluzjaProgramSzostyZmysl)
                     (state final))))						 
					 
(defrule ProgBezFunkcji ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 No))
   (logical (funkcjonalnosc2 No))
   (logical (funkcjonalnosc3 No))
   =>

   (assert (UI-state (display KonkluzjaPranieBezFunkcji)
                     (state final))))	
					 
(defrule ProgZFunkcjami ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 Yes))
   (logical (funkcjonalnosc2 Yes))
   (logical (funkcjonalnosc3 Yes))
   =>

   (assert (UI-state (display KonkluzjaPranieZFunkcjami)
                     (state final))))						 
					 
(defrule ProgPranieOdziezyDelikatnejPranieReczne ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 Yes))
   (logical (funkcjonalnosc2 Yes))
   (logical (funkcjonalnosc3 No))
   =>

   (assert (UI-state (display KonkluzjaPranieOdziezyDelikatnejPranieReczne)
                     (state final))))		

(defrule ProgPranieOdziezyDelikatnejSzostyZmysl ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 Yes))
   (logical (funkcjonalnosc2 No))
   (logical (funkcjonalnosc3 Yes))
   =>

   (assert (UI-state (display KonkluzjaPranieOdziezyDelikatnejSzostyZmysl)
                     (state final))))	

(defrule ProgKonkluzjaPranieReczneSzostyZmysl ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc Yes))
   (logical (funkcjonalnosc1 No))
   (logical (funkcjonalnosc2 Yes))
   (logical (funkcjonalnosc3 Yes))
   =>

   (assert (UI-state (display KonkluzjaPranieReczneSzostyZmysl)
                     (state final))))	
					 
;;;----PYTANIA O DODATKOWE FUNKCJE-----				 

(defrule determine-dodatki ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc No))

   =>

   (assert (UI-state (display Pytanie10)
                     (relation-asserted dodatki)
                     (response No)
                     (valid-answers No Yes)))) 	

						 
(defrule determine-dodatki1 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc No))
 (logical (dodatki Yes))

   =>

   (assert (UI-state (display Pytanie11)
                     (relation-asserted dodatki1)
                     (response No)
                     (valid-answers No Yes)))) 		

(defrule determine-dodatki2 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc No))
 (logical (dodatki Yes))

   =>

   (assert (UI-state (display Pytanie12)
                     (relation-asserted dodatki2)
                     (response No)
                     (valid-answers No Yes)))) 	
					 
(defrule determine-dodatki3 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc No))
 (logical (dodatki Yes))

   =>

   (assert (UI-state (display Pytanie13)
                     (relation-asserted dodatki3)
                     (response Yes)
                     (valid-answers No Yes)))) 		

(defrule determine-dodatki4 ""

 (logical (rocznik No))
 (logical (PodstPytania No))
 (logical (funkcjonalnosc No))
 (logical (dodatki Yes))

   =>

   (assert (UI-state (display Pytanie14)
                     (relation-asserted dodatki4)
                     (response Yes)
                     (valid-answers No Yes)))) 
					 
;;;----WNIOSKOWANIE DODATKOWE FUNKCJE-----			

(defrule WodaHalasFOpoznionyStartFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 Yes))
   (logical (dodatki3 Yes))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaWodaHalasFOpoznionyStartFZablokujKlawisze)
                     (state final))))	 	

(defrule BrakWodaHalasFOpoznionyStartFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 No))
   (logical (dodatki3 No))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaBrakWodaHalasFOpoznionyStartFZablokujKlawisze)
                     (state final))))	 

(defrule Woda ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 No))
   (logical (dodatki3 No))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaWoda)
                     (state final))))

(defrule Halas ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 Yes))
   (logical (dodatki3 No))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaHalas)
                     (state final))))	

(defrule FOpoznionyStart ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 No))
   (logical (dodatki3 Yes))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaFOpoznionyStart)
                     (state final))))	

(defrule FZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 No))
   (logical (dodatki3 No))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaFZablokujKlawisze)
                     (state final))))	

(defrule WodaHalas ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 Yes))
   (logical (dodatki3 No))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaWodaHalas)
                     (state final))))	

(defrule WodaFOpoznionyStart ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 No))
   (logical (dodatki3 Yes))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaWodaFOpoznionyStart)
                     (state final))))		

(defrule WodaFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 No))
   (logical (dodatki3 No))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaWodaFZablokujKlawisze)
                     (state final))))		

(defrule HalasFOpoznionyStart ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 Yes))
   (logical (dodatki3 Yes))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaHalasFOpoznionyStart)
                     (state final))))	

(defrule HalasFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 Yes))
   (logical (dodatki3 No))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaHalasFZablokujKlawisze)
                     (state final))))	

(defrule FOpoznionyStartFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 No))
   (logical (dodatki3 Yes))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaFOpoznionyStartFZablokujKlawisze)
                     (state final))))	

(defrule WodaHalasFOpoznionyStart ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 Yes))
   (logical (dodatki3 Yes))
   (logical (dodatki4 No))
   =>

   (assert (UI-state (display KonkluzjaWodaHalasFOpoznionyStart)
                     (state final))))	

(defrule WodaHalasFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 Yes))
   (logical (dodatki3 No))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaWodaHalasFZablokujKlawisze)
                     (state final))))	

(defrule WodaFOpoznionyStartFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 Yes))
   (logical (dodatki2 No))
   (logical (dodatki3 Yes))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaWodaFOpoznionyStartFZablokujKlawisze)
                     (state final))))	

(defrule HalasFOpoznionyStartFZablokujKlawisze ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki Yes))
   (logical (dodatki1 No))
   (logical (dodatki2 Yes))
   (logical (dodatki3 Yes))
   (logical (dodatki4 Yes))
   =>

   (assert (UI-state (display KonkluzjaHalasFOpoznionyStartFZablokujKlawisze)
                     (state final))))						 

;;;----WNIOSKOWANIE GDY BRAK ODPOWIEDZI-----					 
					 
(defrule brakOdp ""

   (logical (rocznik No))
   (logical (PodstPytania No))
   (logical (funkcjonalnosc No))
   (logical (dodatki No))
   =>

   (assert (UI-state (display KonkluzjabrakOdp)
                     (state final))))						 
					 
;;;---------------------------------------------------------------------------------------
;;;------------------------------- STEROWNICZE -------------------------------------------
;;;---------------------------------------------------------------------------------------


(defrule rule_dodaj
	?i <- (dodaj)
	?j <- (licznik_oznak ?x)
=>
	(retract ?i ?j)
	(assert 
		(licznik_oznak (+ ?x 1))
	)
)

(defrule rule_odejmij
	?i <- (odejmij)
	?j <- (licznik_oznak ?x)
=>
	(retract ?i ?j)
	(assert 
		(licznik_oznak (- ?x 1))
	)
)

(defrule rule_void
	?i <- (bez_zmian)
=>
	(retract ?i)
)

					 
;;;*************************
;;;* GUI INTERACTION RULES *
;;;*************************

(defrule ask-question

   (declare (salience 5))
   
   (UI-state (id ?id))
   
   ?f <- (state-list (sequence $?s&:(not (member$ ?id ?s))))
             
   =>
   
   (modify ?f (current ?id)
              (sequence ?id ?s))
   
   (halt))

(defrule handle-next-no-change-none-middle-of-chain

   (declare (salience 10))
   
   ?f1 <- (next ?id)

   ?f2 <- (state-list (current ?id) (sequence $? ?nid ?id $?))
                      
   =>
      
   (retract ?f1)
   
   (modify ?f2 (current ?nid))
   
   (halt))

(defrule handle-next-response-none-end-of-chain

   (declare (salience 10))
   
   ?f <- (next ?id)

   (state-list (sequence ?id $?))
   
   (UI-state (id ?id)
             (relation-asserted ?relation))
                   
   =>
      
   (retract ?f)

   (assert (add-response ?id)))   

(defrule handle-next-no-change-middle-of-chain

   (declare (salience 10))
   
   ?f1 <- (next ?id ?response)

   ?f2 <- (state-list (current ?id) (sequence $? ?nid ?id $?))
     
   (UI-state (id ?id) (response ?response))
   
   =>
      
   (retract ?f1)
   
   (modify ?f2 (current ?nid))
   
   (halt))

(defrule handle-next-change-middle-of-chain

   (declare (salience 10))
   
   (next ?id ?response)

   ?f1 <- (state-list (current ?id) (sequence ?nid $?b ?id $?e))
     
   (UI-state (id ?id) (response ~?response))
   
   ?f2 <- (UI-state (id ?nid))
   
   =>
         
   (modify ?f1 (sequence ?b ?id ?e))
   
   (retract ?f2))
   
(defrule handle-next-response-end-of-chain

   (declare (salience 10))
   
   ?f1 <- (next ?id ?response)
   
   (state-list (sequence ?id $?))
   
   ?f2 <- (UI-state (id ?id)
                    (response ?expected)
                    (relation-asserted ?relation))
                
   =>
      
   (retract ?f1)

   (if (neq ?response ?expected)
      then
      (modify ?f2 (response ?response)))
      
   (assert (add-response ?id ?response)))   

(defrule handle-add-response

   (declare (salience 10))
   
   (logical (UI-state (id ?id)
                      (relation-asserted ?relation)))
   
   ?f1 <- (add-response ?id ?response)
                
   =>
      
   (str-assert (str-cat "(" ?relation " " ?response ")"))
   
   (retract ?f1))   

(defrule handle-add-response-none

   (declare (salience 10))
   
   (logical (UI-state (id ?id)
                      (relation-asserted ?relation)))
   
   ?f1 <- (add-response ?id)
                
   =>
      
   (str-assert (str-cat "(" ?relation ")"))
   
   (retract ?f1))   

(defrule handle-prev

   (declare (salience 10))
      
   ?f1 <- (prev ?id)
   
   ?f2 <- (state-list (sequence $?b ?id ?p $?e))
                
   =>
   
   (retract ?f1)
   
   (modify ?f2 (current ?p))
   
   (halt))
   
